import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TodolistComponent } from './todolist/todolist.component';
import { InputAutoWidthDirective } from './input-auto-width.directive';
import { MyuppercasePipe } from './myuppercase.pipe';
import { ListboxComponent } from './listbox/listbox.component';
import { ChucknorrisService } from './chucknorris.service';
import { AboutComponent } from './about/about.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { RandomGuard } from './random.guard';

@NgModule({
  declarations: [
    AppComponent,
    TodolistComponent,
    InputAutoWidthDirective,
    MyuppercasePipe,
    ListboxComponent,
    AboutComponent,
    NotfoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: TodolistComponent},
      { path: 'about', component: AboutComponent, canActivate: [RandomGuard]},
      { path: '**', component: NotfoundComponent}
    ])
  ],
  providers: [ChucknorrisService, RandomGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
