import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: 'input.[appInputAutoWidth]'
})
export class InputAutoWidthDirective {
  inputBox: HTMLInputElement;

  @Input()
  appInputAutoWidth: string;

  constructor(elementRef: ElementRef) {
    this.inputBox = elementRef.nativeElement;
  }

  @HostListener('focus')
  onFocus() {
    const width = this.inputBox.offsetWidth + +this.appInputAutoWidth;
    this.inputBox.style.width = width + 'px';
  }

  @HostListener('blur')
  lostFocus() {
    const width = this.inputBox.offsetWidth - Number.parseInt(this.appInputAutoWidth);
    this.inputBox.style.width = width + 'px';
  }

}
