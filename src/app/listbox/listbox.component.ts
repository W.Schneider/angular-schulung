import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-listbox',
  templateUrl: './listbox.component.html',
  styleUrls: ['./listbox.component.css']
})
export class ListboxComponent {

  @Input()
  editIndex;

  @Input()
  items = [];

  @Output()
  editItemClick = new EventEmitter<number>();

  @Output()
  removeItemClick = new EventEmitter<number>();


  editItem(itemIndex: number) {
      this.editItemClick.emit(itemIndex);
  }

  removeItem(itemIndex: number) {
      this.removeItemClick.emit(itemIndex);
  }

}
