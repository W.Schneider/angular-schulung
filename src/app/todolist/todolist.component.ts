import { Component} from '@angular/core';
import { ChucknorrisService } from '../chucknorris.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent {
  todoText = 'hello';
  todos: string[] = [];
  isEditMode = false;
  editTodoIndex = -1;
  jokeText = '';

  constructor(private chucknorrisService: ChucknorrisService, private router: Router) {}

  goToAboutPage() {
    this.router.navigate(['about']);
  }

  addTodo() {
    this.todos.push(this.todoText);
    this.todoText = '';
  }

  removeTodo(todoIndex) {
    this.todos.splice(todoIndex, 1);
    this.chucknorrisService.getJoke().subscribe(joke => {
      this.jokeText = joke.value;
    });
  }

  updateTodo() {
    this.isEditMode = false;
    this.todos[this.editTodoIndex] = this.todoText;
    this.todoText = '';
  }

  editTodo(todoIndex) {
    this.isEditMode = true;
    this.todoText = this.todos[todoIndex];
    this.editTodoIndex = todoIndex;
  }
}
